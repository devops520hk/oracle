
cd /root/
sudo yum install perl -y && \
sudo yum -y groupinstall "Development Tools" && \
yum -y install gcc zlib zlib-devel libffi libffi-devel readline-devel && \
sudo yum -y install glibc-kernheaders && \
sudo yum install -y cmake git gcc gcc-c++ ncurses-devel glibc-kernheaders && \

cd /root/ && \
wget https://www.openssl.org/source/openssl-3.1.3.tar.gz --no-check-certificate && \
tar xvf openssl-3.1.3.tar.gz && \
cd openssl-3.1.3 && \
./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl && \
sudo make  && \
sudo make install && \
sudo mv /usr/bin/openssl /usr/bin/openssl.bak 
sudo mv /usr/bin/include/openssl /usr/include/openssl.bak 
sudo ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl && \
sudo ln -s /usr/local/ssl/include/openssl /usr/bin/include/openssl 
sudo ln -s /usr/local/ssl/lib64/libssl.so.3 /usr/lib/libssl.so.3 && \
sudo ln -s /usr/local/ssl/lib64/libcrypto.so.3 /usr/lib/libcrypto.so.3 && \
sudo echo "export PATH=/usr/local/ssl/bin:\$PATH">>/etc/profile && \
sudo echo "export LD_LIBRARY_PATH=/usr/local/ssl/lib:\$LD_LIBRARY_PATH">>/etc/profile && \
source /etc/profile && \
sudo ldconfig && \
cd /root/ && \
wget https://mirrors.huaweicloud.com/python/3.11.6/Python-3.11.6.tgz && \
tar -zxf Python-3.11.6.tgz && \
cd Python-3.11.6/ && chmod +x configure && mkdir -p /usr/local/python3 && \
./configure --prefix=/usr/local/python3 --with-zlib=/usr/local/ --with-openssl-rpath=auto  --with-openssl=/usr/local/ssl  OPENSSL_LDFLAGS=-L/usr/local/ssl   OPENSSL_LIBS=-l/usr/local/ssl/ssl OPENSSL_INCLUDES=-I/usr/local/ssl && \
sudo make  && \
sudo make install && \
sudo rm -f /usr/bin/python3 && sudo rm -f /usr/bin/pip3 && \
sudo ln -s /usr/local/python3/bin/python3  /usr/bin/python3 && \
sudo ln -s /usr/local/python3/bin/pip3  /usr/bin/pip3 && \
cd /root && \
sudo mkdir -p /root/.pip && \
sudo echo "[global]" >> /root/.pip/pip.conf && \
sudo echo "index-url = http://mirrors.aliyun.com/pypi/simple/"  >> /root/.pip/pip.conf && \
sudo echo "[install]" >> /root/.pip/pip.conf && \
sudo echo "trusted-host = mirrors.aliyun.com" >> /root/.pip/pip.conf && \
cd /root && \
sudo mkdir -p /root/py3venv/ && \
python3 -m venv py3venv/ && \
source /root/py3venv/bin/activate && \
deactivate && \
cd /root  && \
sudo wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo && \
sudo yum -y install docker-ce && \
sudo systemctl start docker && sudo systemctl enable docker && \
docker run hello-world
