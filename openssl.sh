cd /root/
sudo yum install perl -y && \
sudo yum -y groupinstall "Development Tools" && \
yum -y install gcc zlib zlib-devel libffi libffi-devel readline-devel && \
sudo yum -y install glibc-kernheaders && \
sudo yum install -y cmake git gcc gcc-c++ ncurses-devel glibc-kernheaders && \

cd /root/ && \
wget https://www.openssl.org/source/openssl-3.1.3.tar.gz --no-check-certificate && \
tar xvf openssl-3.1.3.tar.gz && \
cd openssl-3.1.3 && \
./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl && \
sudo make  && \
sudo make install && \
sudo mv /usr/bin/openssl /usr/bin/openssl.bak 
sudo mv /usr/bin/include/openssl /usr/include/openssl.bak 
sudo ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl && \
sudo ln -s /usr/local/ssl/include/openssl /usr/bin/include/openssl 
sudo ln -s /usr/local/ssl/lib64/libssl.so.3 /usr/lib/libssl.so.3 && \
sudo ln -s /usr/local/ssl/lib64/libcrypto.so.3 /usr/lib/libcrypto.so.3 && \
sudo echo "export PATH=/usr/local/ssl/bin:\$PATH">>/etc/profile && \
sudo echo "export LD_LIBRARY_PATH=/usr/local/ssl/lib:\$LD_LIBRARY_PATH">>/etc/profile && \
source /etc/profile && \
sudo ldconfig