cd /root && \
sudo wget https://mirrors.huaweicloud.com/mysql/Downloads/MySQL-8.0/mysql-8.0.29-linux-glibc2.12-x86_64.tar && \
sudo mkdir -pv /hongtao/{data,log}/mysql80 && \
sudo mkdir -pv /hongtao/soft && \
sudo tar -xf mysql-8.0.29-linux-glibc2.12-x86_64.tar && \
sudo tar -xf mysql-8.0.29-linux-glibc2.12-x86_64.tar.xz -C /hongtao/soft/  && \
sudo ln -sv /hongtao/soft/mysql-8.0.29-linux-glibc2.12-x86_64  /hongtao/soft/mysql80   && \
sudo echo -e '#!'"$SHELL" >> /etc/profile.d/mysql.sh && \
sudo echo "export mysql=/hongtao/soft/mysql80/" >> /etc/profile.d/mysql.sh && \
sudo echo -e "export PATH=\$PATH:\$mysql/bin" >> /etc/profile.d/mysql.sh && \
source /etc/profile.d/mysql.sh && \

useradd -r -s /sbin/nologin/   -d  /hongtao/data/mysql80/   -c 'MySQL DataBase Server User'  mysql   && \
sudo chown -R mysql:mysql  /hongtao/{soft,data,log}  && \
sudo yum remove -y mariadb-libs && \
sudo rm -rf /etc/my.cnf*  && \
mysqld --initialize-insecure --user=mysql --basedir=/hongtao/soft/mysql80/ --datadir=/hongtao/data/mysql80 && \

sudo cp /hongtao/soft/mysql80/support-files/mysql.server  /etc/init.d/mysqld && \
sudo sed -i -r "s#^(basedir=)#\1/hongtao/soft/mysql80#"   /etc/init.d/mysqld && \
sudo sed -i -r "s#^(datadir=)#\1/hongtao/data/mysql80#"   /etc/init.d/mysqld && \
sudo chkconfig --add mysqld && \
sudo systemctl daemon-reload && \
sudo systemctl restart mysqld && \
sudo systemctl status mysqld


