#!/bin/bash
sudo sed -i '17a Port 28560' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?Port.*$/Port 22/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?ClientAliveInterval.*/ClientAliveInterval 3600/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?ClientAliveCountMax.*/ClientAliveCountMax 10/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?MaxSessions.*/MaxSessions 10/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?UseDNS.*/UseDNS no/g' /etc/ssh/sshd_config && \
sudo service sshd restart && \

sed -i -e "s|mirrorlist=|#mirrorlist=|g" /etc/yum.repos.d/CentOS-*  && \
sed -i -e "s|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g" /etc/yum.repos.d/CentOS-*  && \
sudo yum update -y 
sudo yum clean all && \
sudo yum makecache && \
sudo yum update -y 
sed -i -e "s|mirrorlist=|#mirrorlist=|g" /etc/yum.repos.d/CentOS-*  && \
sed -i -e "s|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g" /etc/yum.repos.d/CentOS-*  && \
sudo yum -y install wget && sudo yum install -y curl && \

sudo systemctl stop firewalld.service && \
sudo systemctl disable firewalld.service && \

sudo localectl set-locale LANG=en_US.utf8 && \
sudo timedatectl set-timezone Asia/Shanghai && \
sudo hostnamectl set-hostname devops520 && \

sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "####This is ps1 profile ###\n"  >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo "export PS1=\"\[\033[0;31m\]\342\224\214\342\224\200$([[ $? != 0 ]] && echo "[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200")\[\e[33m\]☭ \[\e[m\] \[\e[31m\][\[\e[m\]\[\e[35m\]\u\[\e[m\] ☯ \[\033[01;96m\]\h\[\033[0;31m\] \[\e[35m\]✿\[\e[m\] \[\e[34m\]\t\[\e[m\]\[\e[31m\]]\[\e[m\] \342\224\200 \[\e[31m\][\[\e[m\]\[\e[33m\]PATH:➜ \[\e[m\]\[\033[0;32m\]\w\[\033[0;31m\]] \342\224\200 \[\e[31m\][\[\e[m\]\[\033[0;36m\]$(cat /etc/os-release | grep 'PRETTY_NAME=' | awk -F '"' '{print $2}')\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\\\$\[\e[0m\] \"" >>    /etc/profile   && \
source /etc/profile && \



sudo yum -y install https://packages.endpointdev.com/rhel/8/main/x86_64/endpoint-repo.noarch.rpm && \
sudo yum -y install git && \
sudo yum -y install glibc-kernheaders && \
sudo yum install -y cmake git  python3 python3-devel gcc gcc-c++ ncurses-devel glibc-kernheaders && \
sudo git clone https://gitee.com/zalois/vim.git && \
cd vim/ && \
sudo ./configure && \
sudo make  && \
sudo sudo make install && \
sudo yum -y remove vim && \
sudo cp ~/vim/src/vim  /usr/bin/ && \

cd /root/
sudo yum install perl -y && \
sudo yum -y groupinstall "Development Tools" && \
yum -y install gcc zlib zlib-devel libffi libffi-devel readline-devel && \
sudo yum -y install glibc-kernheaders && \
sudo yum install -y cmake git gcc gcc-c++ ncurses-devel glibc-kernheaders && \

cd /root/ && \
wget https://www.openssl.org/source/openssl-3.1.3.tar.gz --no-check-certificate && \
tar xvf openssl-3.1.3.tar.gz && \
cd openssl-3.1.3 && \
./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl && \
sudo make  && \
sudo make install && \
sudo mkdir -p /usr/bin/include/ && \
sudo mv /usr/bin/openssl /usr/bin/openssl.bak 
sudo mv /usr/bin/include/openssl /usr/include/openssl.bak 
sudo ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl && \
sudo ln -s /usr/local/ssl/include/openssl /usr/bin/include/openssl 
sudo ln -s /usr/local/ssl/lib64/libssl.so.3 /usr/lib/libssl.so.3 && \
sudo ln -s /usr/local/ssl/lib64/libcrypto.so.3 /usr/lib/libcrypto.so.3 && \
sudo echo "export PATH=/usr/local/ssl/bin:\$PATH">>/etc/profile && \
sudo echo "export LD_LIBRARY_PATH=/usr/local/ssl/lib:\$LD_LIBRARY_PATH">>/etc/profile && \
source /etc/profile && \
sudo ldconfig && \

cd /root/ && \
wget https://www.sqlite.org/2023/sqlite-autoconf-3440000.tar.gz && \
tar -xvzf sqlite-autoconf-3440000.tar.gz && \
cd sqlite-autoconf-3440000 && \
./configure --prefix=/usr/local/sqlite3 && \
make && make install && \
mv /usr/bin/sqlite3  /usr/bin/sqlite3.bak && \
ln /usr/local/sqlite3/bin/sqlite3   /usr/bin/sqlite3 && \

yum -y update && \
yum -y install sqlite-devel && \
yum -y install gcc zlib zlib-devel libffi libffi-devel readline-devel && \

wget https://mirrors.huaweicloud.com/python/3.11.6/Python-3.11.6.tgz && \
tar -zxf Python-3.11.6.tgz && \
cd Python-3.11.6/ && chmod +x configure && mkdir -p /usr/local/python3 && \
cd /root/Python-3.*/ && chmod +x configure && mkdir -p /usr/local/python3 && \
./configure --prefix=/usr/local/python3 --with-sqlite3=/usr/local/sqlite3 --with-zlib=/usr/local/ --with-openssl-rpath=auto  --with-openssl=/usr/local/openssl  OPENSSL_LDFLAGS=-L/usr/local/openssl   OPENSSL_LIBS=-l/usr/local/openssl/ssl OPENSSL_INCLUDES=-I/usr/local/openssl  && \
sudo make install && \
sudo rm -f /usr/bin/python3 && sudo rm -f /usr/bin/pip3 && \
sudo ln -s /usr/local/python3/bin/python3  /usr/bin/python3 && \
sudo ln -s /usr/local/python3/bin/pip3  /usr/bin/pip3 && \
cd /root && \
sudo mkdir -p /root/.pip && \
sudo echo "[global]" >> /root/.pip/pip.conf && \
sudo echo "index-url = http://mirrors.aliyun.com/pypi/simple/"  >> /root/.pip/pip.conf && \
sudo echo "[install]" >> /root/.pip/pip.conf && \
sudo echo "trusted-host = mirrors.aliyun.com" >> /root/.pip/pip.conf && \
cd /root && \
sudo mkdir -p /root/py3venv/ && \
python3 -m venv py3venv/ && \
source /root/py3venv/bin/activate && \
deactivate && \
cd /root  && \
sudo wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo && \
sudo yum -y install docker-ce && \
sudo systemctl start docker && sudo systemctl enable docker && \
docker run hello-world
