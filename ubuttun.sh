#!/bin/bash
sudo sed -i '17a Port 28560' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?Port.*$/Port 22/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?ClientAliveInterval.*/ClientAliveInterval 3600/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?ClientAliveCountMax.*/ClientAliveCountMax 10/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?MaxSessions.*/MaxSessions 10/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config && \
sudo sed -i 's/^#\?UseDNS.*/UseDNS no/g' /etc/ssh/sshd_config && \
sudo service sshd restart && \

sudo systemctl stop firewalld.service && \
sudo systemctl disable firewalld.service && \

sudo localectl set-locale LANG=en_US.utf8 && \
sudo timedatectl set-timezone Asia/Shanghai && \
sudo hostnamectl set-hostname devops520 && \

sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "####This is ps1 profile ###\n"  >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo "export PS1=\"\[\033[0;31m\]\342\224\214\342\224\200$([[ $? != 0 ]] && echo "[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200")\[\e[33m\]☭ \[\e[m\] \[\e[31m\][\[\e[m\]\[\e[35m\]\u\[\e[m\] ☯ \[\033[01;96m\]\h\[\033[0;31m\] \[\e[35m\]✿\[\e[m\] \[\e[34m\]\t\[\e[m\]\[\e[31m\]]\[\e[m\] \342\224\200 \[\e[31m\][\[\e[m\]\[\e[33m\]PATH:➜ \[\e[m\]\[\033[0;32m\]\w\[\033[0;31m\]] \342\224\200 \[\e[31m\][\[\e[m\]\[\033[0;36m\]$(cat /etc/os-release | grep 'PRETTY_NAME=' | awk -F '"' '{print $2}')\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\\\$\[\e[0m\] \"" >>    /etc/profile   && \
source /etc/profile && \



sudo apt-get  clean all && \
sudo apt-get  makecache && \
sudo apt-get  update -y && \
sudo apt-get  -y install wget && sudo  apt-get  install -y curl && \

sudo apt-get  -y install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo.x86_64.rpm && \
sudo apt-get  -y install git && \
sudo apt-get  -y install glibc-kernheaders && \
sudo apt-get  install -y cmake git  python3 python3-devel gcc gcc-c++ ncurses-devel glibc-kernheaders && \
sudo git clone https://gitee.com/zalois/vim.git && \
cd vim/ && \
sudo ./configure && \
sudo make -j8 && \
sudo sudo make install && \
sudo apt-get  -y remove vim && \
sudo cp ~/vim/src/vim  /usr/bin/ && \

cd /root && \
sudo  apt-get  -y groupinstall "Development Tools" && \
wget https://www.openssl.org/source/openssl-1.1.1w.tar.gz --no-check-certificate && \
tar xvf openssl-1.1.1w.tar.gz && \
cd openssl-1.1.1w && \
./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl && \
sudo make -j $(nproc) && \
sudo make install && \
sudo ldconfig && \
sudo echo "export PATH=/usr/local/openssl/bin:\$PATH">>/etc/profile.d/openssl.sh && \
sudo echo "export LD_LIBRARY_PATH=/usr/local/openssl/lib:\$LD_LIBRARY_PATH">>/etc/profile.d/openssl.sh && \
source /etc/profile.d/openssl.sh &&\
cd /root && \
sudo apt-get  -y update && \
sudo apt-get  -y install gcc zlib zlib-devel libffi libffi-devel readline-devel && \
wget https://mirrors.huaweicloud.com/python/3.11.6/Python-3.11.6.tgz && \
tar -zxf Python-3.11.6.tgz && \
cd Python-3.11.6/ && chmod +x configure && mkdir -p /usr/local/python3 && \
./configure --prefix=/usr/local/python3 --with-zlib=/usr/local/ --with-openssl-rpath=auto  --with-openssl=/usr/local/openssl  OPENSSL_LDFLAGS=-L/usr/local/openssl   OPENSSL_LIBS=-l/usr/local/openssl/ssl OPENSSL_INCLUDES=-I/usr/local/openssl && \
sudo make -j 4 && \
sudo make install && \
sudo rm -f /usr/bin/python3 && sudo rm -f /usr/bin/pip3 && \
sudo ln -s /usr/local/python3/bin/python3  /usr/bin/python3 && \
sudo ln -s /usr/local/python3/bin/pip3  /usr/bin/pip3 && \
cd /root && \
sudo mkdir -p /root/.pip && \
sudo echo "[global]" >> /root/.pip/pip.conf && \
sudo echo "index-url=https://pypi.tuna.tsinghua.edu.cn/simple"  >> /root/.pip/pip.conf && \
sudo echo "trusted-host = pypi.tuna.tsinghua.edu.cn" >> /root/.pip/pip.conf && \
cd /root && \
sudo mkdir -p /root/py3venv/ && \
python3 -m venv py3venv/ && \
source /root/py3venv/bin/activate && \
deactivate && \
cd /root  && \
sudo wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo && \
sudo apt-get  -y install docker-ce && \
sudo systemctl start docker && sudo systemctl enable docker && \
docker run hello-world
