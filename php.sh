sudo yum update -y && \
sudo yum -y groupinstall "Development Tools" && \
sudo yum -y install libxml2-devel sqlite-devel bzip2-devel libcurl-devel libffi-devel libpng-devel libwebp-devel libjpeg-devel libxml2-devel libcurl-devel openssl openssl-devel libpng-devel bzip2 postgresql-devel libxml2 && \

cd /root/
sudo git clone https://gitee.com/mirrors/CMake.git && \
cd /root/CMake/ 
./bootstrap --prefix=/usr/local/cmake --datadir=share/cmake --docdir=doc/cmake && \
sudo make && sudo  make install && \
ln -s /usr/local/cmake/bin/cmake /usr/local/bin/cmake && \
ln -s /usr/local/cmake/bin/cpack /usr/local/bin/cpack && \
ln -s /usr/local/cmake/bin/ctest /usr/local/bin/ctest && \


cd /root/
sudo git clone https://gitee.com/mirrors/libzip.git && \
sudo mkdir -p /root/libzip/build/ && \
cd /root/libzip/build/ 
cmake -DCMAKE_INSTALL_PREFIX=/usr .. && \
sudo make && \
sudo make install && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "PKG_CONFIG_PATH=\$PKG_CONFIG_PATH:/usr/lib/pkgconfig:/usr/lib64/pkgconfig:/usr/local/lib64/pkgconfig" >> /etc/profile && \
sudo echo "export PKG_CONFIG_PATH" >> /etc/profile && \
source /etc/profile && \


cd /root/ 
sudo git clone https://gitee.com/mirrors/oniguruma.git && \
cd /root/oniguruma/
./autogen.sh && \
./configure --prefix=/usr --libdir=/lib64 && \
sudo make && sudo make install && \



cd /root/
sudo wget https://www.php.net/distributions/php-8.2.12.tar.gz && \
sudo tar -zvxf php-8.2.12.tar.gz && \
cd /root/php-8.2.12/


./configure --prefix=/usr/local/php --sysconfdir=/etc/php --with-zlib=/usr/local/ --with-sqlite3=/usr/local/sqlite3  --with-bz2  --with-curl --enable-bcmath  --enable-gd   --with-webp   --with-jpeg   --with-mhash   --enable-mbstring   --with-imap-ssl   --with-mysqli   --enable-exif   --with-ffi   --with-zip   --enable-sockets   --with-pcre-jit   --enable-fpm   --with-pdo-mysql    --with-pdo-pgsql --enable-pcntl  --with-openssl-rpath=auto   --with-oniguruma --with-libzip --disable-fileinfo && \
sudo make && sudo  make install && \

sudo groupadd www &&  useradd -g www -s /sbin/nologin www && \
cp /root/php-8.2.12/php.ini-development /usr/local/php/lib/php.ini && \
cp /etc/php/php-fpm.conf.default /etc/php/php-fpm.conf && \
cp /etc/php/php-fpm.d/www.conf.default /etc/php/php-fpm.d/www.conf && \

sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "\n" >> /etc/profile && \
sudo echo -e "export PATH=\$PATH:/usr/local/php/bin" >> /etc/profile && \
source /etc/profile && \

cd /root/ 
sudo mkdir /usr/local/php/tmp && chmod -R 755 /usr/local/php/tmp && \
sudo sed -i.bak 's#session.save_path = "/tmp"#session.save_path = "/usr/local/php/tmp"#g' /usr/local/php/lib/php.ini  && \
sudo sed -i.bak 's#expose_php = On#expose_php = Off#g' /usr/local/php/lib/php.ini && \
sudo sed -i.bak 's#user = nobody#user = www#g' /etc/php/php-fpm.d/www.conf && \
sudo sed -i.bak 's#group = nobody#group = www#g' /etc/php/php-fpm.d/www.conf && \


sudo echo "[Unit]" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "Description=php-fpm" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "After=syslog.target network.target" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo -e "\n" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "[Service]" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "Type=forking" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "ExecStart=/usr/local/php/sbin/php-fpm" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo -e "ExecReload=/bin/kill -USR2 \$MAINPID" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "PrivateTmp=true" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo -e "\n" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "[Install]" >> /usr/lib/systemd/system/php-fpm.service && \
sudo echo "WantedBy=multi-user.target" >> /usr/lib/systemd/system/php-fpm.service && \

cd /root/ 
sudo systemctl daemon-reload && \
sudo systemctl start php-fpm && sudo systemctl enable php-fpm && sudo systemctl status php-fpm && \

cd /root/ 
sudo yum install -y gd-devel.x86_64 pcre-devel.x86-64 pcre2-devel.x86_64 ghc-pcre-light.x86_64 opensips-regex.x86_64 && \
sudo wget https://nginx.org/download/nginx-1.22.1.tar.gz  && \
sudo useradd -s /sbin/nologin nginx && \
sudo tar -zxvf nginx-1.22.1.tar.gz && \
cd nginx-1.22.1/
./configure --prefix=/usr/local/nginx --user=nginx --group=nginx --with-mail  --with-openssl=/root/openssl-1.1.1w/ --with-pcre --with-stream --with-threads --with-file-aio --with-http_v2_module --with-http_flv_module --with-http_mp4_module --with-http_ssl_module --with-http_sub_module --with-http_dav_module --with-mail_ssl_module --with-http_slice_module --with-stream_ssl_module --with-http_realip_module --with-http_gunzip_module --with-http_addition_module --with-http_secure_link_module --with-http_stub_status_module --with-http_gzip_static_module --with-http_random_index_module --with-http_auth_request_module --with-http_image_filter_module  && \
sudo make && sudo make install && \

cd /root/ 
sudo echo "[Unit]" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "Description=nginx - high performance web server"  >> /usr/lib/systemd/system/nginx.service && \
sudo echo "Documentation=http://nginx.org/en/docs/" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "After=network.target remote-fs.target nss-lookup.target" >> /usr/lib/systemd/system/nginx.service && \
sudo echo -e "\n" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "[Service]" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "Type=forking" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "PIDFile=/usr/local/nginx/logs/nginx.pid" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "ExecStartPre=/usr/local/nginx/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "ExecReload= /usr/local/nginx/sbin/nginx -s reload" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "ExecStop= /usr/local/nginx/sbin/nginx -s stop" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "PrivateTmp=true" >> /usr/lib/systemd/system/nginx.service && \
sudo echo -e "\n" >> /usr/lib/systemd/system/nginx.service && \
sudo echo "[Install]"  >> /usr/lib/systemd/system/nginx.service && \
sudo echo "WantedBy=multi-user.target"  >> /usr/lib/systemd/system/nginx.service && \
sudo chmod +x /usr/lib/systemd/system/nginx.service  && \
sed -i.bak 's#listen       80;#listen       88;#g' /usr/local/nginx/conf/nginx.conf && \

sudo systemctl daemon-reload && \
sudo systemctl start nginx.service && \
sudo systemctl enable nginx.service && \
sudo systemctl status nginx.service
